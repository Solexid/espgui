// 
// 
// 

#include "CStatusBar.h"

#include "LinkedList.h"
LinkedList<const tPicture*> *icons;
int hh;
int mm;
int ss;
int timestamp;
static uint8_t conv2d(const char* p) {
	uint8_t v = 0;
	if ('0' <= *p && *p <= '9') v = *p - '0';
	return 10 * v + *++p - '0';
}

bool CStatusBar::initApp(void *params)
{
	icons =new LinkedList<const tPicture*>();


	sprintf(Name, "StatusBar");
	Serial.println("StatusBar App init started!");
	FullHeap = ESP.getFreeHeap();
	OnThink();
	timestamp = millis();
	hh = conv2d(__TIME__);
	mm = conv2d(__TIME__ + 3);
	ss = conv2d(__TIME__ + 6);
	isForeground = true;
	isPrimary = true;
	return true;
}

void CStatusBar::OnThink()
{


	
	DrawHeap();
	DrawTime();
	DrawIcons();
	tft.drawRect(0, 0, 128, 16, CYAN);
	NextThink = millis() + 1000;
	Serial.println("CStatusBar App :: Tick!");
	/*
	Serial.print("CStatusBar App :: ");
	Serial.print(sizeof(this));
	Serial.print(" size of this app.");
	Serial.println(result);
	Serial.println();*/

}
void CStatusBar::DrawHeap()
{
	int heap = ESP.getFreeHeap();
	if (pheap != heap)pheap = heap;
	else return;
	if (heap > FullHeap)FullHeap = heap;
	int result = 27;
	if (FullHeap>0)
		result = (((FullHeap - (FullHeap - heap)) / (FullHeap / 100)) * 27) / 100;
	int heapz = heap / 1000;
	tft.fillRect(2, 2, 30, 12, BLUE);
	tft.fillRect(3,3, result, 10, tft.htmlTo565(0x119999));
	tft.setCursor(4, 4, NORM);
	tft.setTextScale(1);
	tft.setTextColor(BLACK);
	tft.print(heapz);
	tft.print(" kb");
}

void CStatusBar::DrawTime()
{
	int milliz = millis() - timestamp;
	int hours = (milliz / 3600000);
	int hourz = hh + hours;
	if (hourz > 23) { hh = 0;; }
	int timez=mm + ((milliz- (hours* 3600000)) / 60000);
	if (timez > 60) { mm = 0; hh++; }


	tft.setCursor(60, 5);
	tft.setTextScale(1);
	tft.setTextColor(tft.htmlTo565(0x119999),BLACK);
//	tft.print(":");
//	tft.print(ss);
	tft.print(hourz);
	tft.print(":");
	tft.print(timez);
	tft.setTextScale(1);
	tft.setTextColor(WHITE);
}
void CStatusBar::DrawIcons()
{
	for (int i = 0; i < icons->size(), i <3; i++)

	{
		const tPicture* icon = icons->at(i);
		if (icon == NULL) break;
		tft.drawImage(112-(i*16), 0, icon);

	}


	
}


