// CDebugHelper.h

#ifndef _CDEBUGHELPER_h
#define _CDEBUGHELPER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "CApp.h"
class CDebugHelper : public CApp
{
 protected:


 public:
	 bool initApp(void* params);
	 void OnThink();
};


#endif

