// CWifiApp.h

#ifndef _CWifiApp_h
#define _CWifiApp_h


#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "CApp.h"



#include <WiFiUdp.h>
#include <ESP8266WiFi.h>
class CWifiApp : public CApp
{
 protected:
	 bool first;
	 bool started;
	 unsigned long sendNTPpacket(IPAddress& address);
	 WiFiUDP udp;
	 void DrawWifiStatus();

 public:
	 int FullHeap;
	 bool initApp(void* params);
	 void OnThink();
};


#endif

