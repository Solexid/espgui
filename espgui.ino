
#include <IRremoteESP8266.h>
#define _FORCE_PROGMEM__ true
#include "GUI.h"
#include "CStatusBar.h"
#include "CDebugHelper.h"
#include "CTaskMon.h"
#include "CApp.h"
#include "CInputMan.h"
#include "CWifiApp.h"
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>
#include <gfxfont.h>
#include <Adafruit_GFX.h>
#include <SPI.h>
#include <TFT_ST7735.h>
#define __CS  4 //(D0)
#define __DC  5   //(D1)
#define __RST 16  //(D2)
#include <ESP8266WebServer.h>
#include "LinkedList.h"
TFT_ST7735 tft = TFT_ST7735(__CS, __DC, __RST);



CApp* ActiveApp;


pr_buttons buttnmgr;


extern int B_UP ;
extern int B_DOWN ;
extern int B_LEFT ;
extern int B_RIGHT;
extern int B_OK ;
extern int B_OFF;
extern int B_LB;
extern int B_RB;

LinkedList<CApp *>  *TaskList ;

void setup()
{
	TaskList = (new LinkedList<CApp *>());
	Serial.begin(115200);
	Serial.print("\nstarted\n");
	tft.begin();
	CDebugHelper *dbg = new CDebugHelper();
	CStatusBar *Bar = new CStatusBar();
	CTaskMon *Mon = new CTaskMon();
	CInputMan *Input = new CInputMan();
	CWifiApp *Wifi = new CWifiApp();
	dbg->initApp(NULL);
	Bar->initApp(NULL);
	Input->initApp(NULL);
	Mon->initApp(NULL);
	Wifi->initApp(NULL);
	ActiveApp = Mon;
	TaskList->push_back( dbg);
	TaskList->push_back(Bar);
	TaskList->push_back(Mon);
	TaskList->push_back(Wifi);
	TaskList->push_back(Input);
}




void loop()
{	
	
	tft.setTextColor(WHITE);
	tft.setTextScale(1);
	tft.changeMode(PARTIAL);
	
	
	
	GreatThinker();

  /* add main program code here */

}

void GreatThinker() {
#     pragma omp parallel num_threads(4)
	for (int i = 0; i < 10; i++)
	
	{	CApp* app= TaskList->at(i);
		if(app == NULL) break;
		

		if (app->NextThink < millis())
		{
			//	Serial.println(app->Name);
		//	Serial.println(app->NextThink);
			app->OnThink();
		}
	}


}


