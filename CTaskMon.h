// CTaskMon.h

#ifndef _CTaskMon_h
#define _CTaskMon_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "CApp.h"
class CTaskMon : public CApp
{
 protected:

	 bool refresh = true;
public:
	GUIClass*  gui = new GUIClass();
	virtual bool initApp(void* params);
	virtual void OnThink();
	virtual	void DestroyApp();
	void RenderFrame();
};


#endif

