// 
// 
// 

#include "CInputMan.h"
int B_UP = 0xFFA05F;
int B_DOWN = 0xFF40BF;
int B_LEFT = 0xFF50AF;
int B_RIGHT = 0xFF7887;
int B_OK = 0xFF02FD;
int B_OFF = 0xFFB24D;
int B_LB = 0xFF32CD;
int B_RB = 0xFF30CF;
IRrecv irrecv(D3);

#include "LinkedList.h"
extern LinkedList<CApp *>  *TaskList;;
bool CInputMan::initApp(void* params)
{
	buttnmgr.check=false;
	buttnmgr.last = 0;
	buttnmgr.now = 0;

	sprintf(Name, "InputMan");
	Serial.print(Name);
	Serial.println(" App init started!");
	NextThink = millis() + 5;
	isForeground = false;
	isPrimary = true;
	//Wire.begin(D6, D3);
	irrecv.enableIRIn();
	Serial.println(" App init finished!");
	//accelgyro.initialize();
	
	//Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
	
	return true;
}


decode_results  results;
void CInputMan::OnThink()
{

	if (isForeground)RenderFrame();

	if (irrecv.decode(&results)) {  // Grab an IR code
		dumpCode(&results);           // Output the results
		
		// Blank line between entries
		irrecv.resume();
	}
	
	NextThink = millis() + 5;
}


void CInputMan::DestroyApp()
{
	delete(this);
}
void CInputMan::RenderFrame()
{
	        // Somewhere to store the results
//	Serial.println("-----------------");

	//tft.fillRect(6, 32, 150, 115, BLACK);


	/*for (int i = 0; i < 4; i++)

	{
		CApp* app = TaskList[i];
		if (app == NULL) break;



		tft.setTextScale(1);
		tft.fillRect(7, 33 + i * 20, 114, 15, tft.htmlTo565(0x119999));
		tft.drawRect(6, 32 + i * 20, 115, 16, BLUE);
		tft.setCursor(12, 36 + i * 20);
		tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
		tft.print(i);
		tft.print("  ");
		tft.print(String(app->Name));
		tft.setCursor(114-32, 36 + i * 20);
		tft.print(app->HeapUsed());
		tft.print(" bytes");

	}*/

}

void CInputMan::RenderAccel() {

	//accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

	//tft.setTextScale(1);
	//tft.fillRect(7, 33 + 0 * 20, 114, 15, tft.htmlTo565(0x119999));
	//tft.drawRect(6, 32 + 0 * 20, 115, 16, BLUE);
	//tft.setCursor(12, 36 + 0 * 20);
	//tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	//tft.print("accelx= ");
	//tft.print(ax);

	//tft.fillRect(7, 33 + 1 * 20, 114, 15, tft.htmlTo565(0x119999));
	//tft.drawRect(6, 32 + 1 * 20, 115, 16, BLUE);
	//tft.setCursor(12, 36 + 1 * 20);
	//tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	//tft.print("accely= ");
	//tft.print(ay);

	//tft.fillRect(7, 33 + 2 * 20, 114, 15, tft.htmlTo565(0x119999));
	//tft.drawRect(6, 32 + 2 * 20, 115, 16, BLUE);
	//tft.setCursor(12, 36 + 2 * 20);
	//tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	//tft.print("accelz= ");
	//tft.print(az);

	//tft.fillRect(7, 33 + 3 * 20, 114, 15, tft.htmlTo565(0x119999));
	//tft.drawRect(6, 32 + 3 * 20, 115, 16, BLUE);
	//tft.setCursor(12, 36 + 3 * 20);
	//tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	//tft.print("gyrox= ");
	//tft.print(gx);

	//tft.fillRect(7, 33 + 4 * 20, 114, 15, tft.htmlTo565(0x119999));
	//tft.drawRect(6, 32 + 4 * 20, 115, 16, BLUE);
	//tft.setCursor(12, 36 + 4 * 20);
	//tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	//tft.print("gyroy= ");
	//tft.print(gy);

	//tft.fillRect(7, 33 + 5 * 20, 114, 15, tft.htmlTo565(0x119999));
	//tft.drawRect(6, 32 + 5 * 20, 115, 16, BLUE);
	//tft.setCursor(12, 36 + 5 * 20);
	//tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	//tft.print("gyroz= ");
	//tft.print(gz);
}
void  CInputMan::dumpCode(decode_results *results)
{/*
	tft.setTextScale(1);
	tft.fillRect(7, 33 + 0 * 20, 114, 15, tft.htmlTo565(0x119999));
	tft.drawRect(6, 32 + 0 * 20, 115, 16, BLUE);
	tft.setCursor(12, 36 + 0 * 20);
	tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	tft.print("NEC VAL= ");
	tft.print(results->value);
	tft.fillRect(7, 33 + 1 * 20, 114, 15, tft.htmlTo565(0x119999));
	tft.drawRect(6, 32 + 1 * 20, 115, 16, BLUE);
	tft.setCursor(12, 36 + 1 * 20);
	tft.setTextColor(BLACK, tft.htmlTo565(0x119999));
	tft.print("BUTTON= ");*/
	/*int data = results->value;

		if (data == B_UP )
		tft.print("UP"); 
		else
		if (data == B_DOWN)
		tft.print("DOWN");
		else;
		if (data == B_LEFT)
		tft.print("LEFT"); 
		else;
		if (data == B_RIGHT)
		tft.print("RIGHT");
		else;
		if (data == B_OK)
		tft.print("OK");
		else;
		if (data == B_LB)
		tft.print("LB"); 
		else;
		if (data == B_RB)
		tft.print("RB"); 
		else;
		if (data == B_OFF)
		tft.print("OFF"); 
*/
		buttnmgr.last = buttnmgr.now;
		if(results->value!=0xFFFFFFFF)
		buttnmgr.now = results->value;
		buttnmgr.check = false;

	//Serial.print("NEC VAL=");
	Serial.print(results->value, HEX);
	Serial.println();
	//Serial.print(" (");
}