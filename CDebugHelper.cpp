// 
// 
// 

#include "CDebugHelper.h"

bool CDebugHelper:: initApp(void* params)
{
	

	sprintf(Name, "DbgHelper");
	Serial.println("Debug App init started!");
	OnThink();
	isForeground = false;
	return true;
}

void CDebugHelper::OnThink()
{
	
	NextThink = millis() + 1000;
	Serial.println("Debug App :: Tick!");
	Serial.print("Debug App :: " );
	Serial.print(ESP.getFreeHeap());
	Serial.print(" bytes free !");
	Serial.println();
	Serial.print("Debug App :: ");
	Serial.print(sizeof(this));
	Serial.print(" size of this app.");
	Serial.println();

}



