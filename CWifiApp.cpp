// 
// 
// 

#include "CWifiApp.h"

#include "LinkedList.h"

#include "include/wifi.c"
#include "include/wifi_client.c"
extern LinkedList<const tPicture*> *icons;

const char *ssid = "SLX-network";
const char *password = "512150451";
IPAddress timeServerIP;
const char* ntpServerName = "ntp2.stratum2.ru";

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

extern int hh;
extern int mm;
extern int ss;
extern int timestamp;

bool isclient = false;
void handleRoot() {
	isclient = true;
	int heap = ESP.getFreeHeap();
	String str = "<h1>Free Heap:" + String(heap);
//	server.send(200, "text/html", str);
}
bool CWifiApp::initApp(void* params)
{
	first = true;
	sprintf(Name, "WifiApp");
	Serial.println("CWifiApp App init started!");
	WiFi.mode(WIFI_STA);
	NextThink = millis() + 300;
	started = false;
	isHasGUI = false;
	isForeground = false;
	isPrimary = true;
	return true;
}
unsigned int localPort = 2390;	
void CWifiApp::OnThink()
{
	isclient = false;
	if (first)
	{
		
		if(!started){
		WiFi.begin(ssid, password);
		started = true;
		}

		if(WiFi.status() == WL_CONNECTED){
		Serial.println("IP address: ");
		Serial.println(WiFi.localIP());
		udp.begin(localPort);

		WiFi.hostByName(ntpServerName, timeServerIP);
		first = false;
		}
	}
	NextThink = millis() + 500;
	if (WiFi.status() == WL_CONNECTED) {

		WiFi.hostByName(ntpServerName, timeServerIP);
		sendNTPpacket(timeServerIP);
		
		int cb = udp.parsePacket();
		if (cb) {
			udp.read(packetBuffer, NTP_PACKET_SIZE);

			unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
			unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);

			unsigned long secsSince1900 = highWord << 16 | lowWord;
			const unsigned long seventyYears = 2208988800UL;
			unsigned long epoch = secsSince1900 - seventyYears;
			hh = (epoch % 86400L) / 3600;
			hh += 3;
			mm = (epoch % 3600) / 60;
			ss = epoch % 60;
			timestamp = millis();
			NextThink = millis() + 60000;
		}
	}
	DrawWifiStatus();


	Serial.println("CWifiApp App :: Tick!");
	/*
	Serial.print("CWifiApp App :: ");
	Serial.print(sizeof(this));
	Serial.print(" size of this app.");
	Serial.println(result);
	Serial.println();*/

}
int status = 0;
void CWifiApp::DrawWifiStatus()
{
	
	if (status != WiFi.status())status = WiFi.status();	else return;

		
	
	if (WiFi.status() == WL_CONNECTED)
	{ 
	icons->remove(&wifi);
	icons->push_back(&wifi_client);
	}
	else
	{
		icons->remove(&wifi_client);
		icons->push_back(&wifi);
	}
}
unsigned long CWifiApp::sendNTPpacket(IPAddress& address)
{
	Serial.println("sending NTP packet...");
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
							 // 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12] = 49;
	packetBuffer[13] = 0x4E;
	packetBuffer[14] = 49;
	packetBuffer[15] = 52;

	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:
	udp.beginPacket(address, 123); //NTP requests are to port 123
	udp.write(packetBuffer, NTP_PACKET_SIZE);
	udp.endPacket();
}

