// CApp.h

#ifndef _CAPP_h
#define _CAPP_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Adafruit_GFX.h>
#include <SPI.h>
#include <TFT_ST7735.h>
extern TFT_ST7735 tft;
 struct pr_buttons
{
	int last;
	int now;
	bool check;

};

extern pr_buttons buttnmgr;
#include "GUI.h"
class CApp
{
 protected:

	 int maxitems;
	 int start;
	 int end;
 public:
	virtual bool initApp(void* params);
	virtual void OnThink();
		unsigned long NextThink;
	virtual	void DestroyApp();
		bool isForeground;
		bool isPrimary;
		bool isHasGUI;
		char Name[16];
	virtual	void RenderFrame(){};
	int HeapUsed();


};

extern CApp* ActiveApp;
#endif

