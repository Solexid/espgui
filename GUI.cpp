// 
// 
// 

#include "GUI.h"

#include "LinkedList.h"
extern int B_UP;
extern int B_DOWN;
extern int B_LEFT;
extern int B_RIGHT;
extern int B_OK;
extern int B_OFF;
extern int B_LB;
extern int B_RB;
#include <WString.h>
void GUIClass::init(int maxitems,int height)
{
	Menu = new LinkedList<menuItem*>();
	maxscreen = 6;
	start = 0;
	end = 6;
	pointer = 0;

}


void GUIClass::MainWindow()
{
	
	for (int i = 0; i < 5; i++)
	{
		tft.setTextScale(1);
		tft.drawRect(6, 32 + i * 20, 100, 16, LIGHT_GREY);
		tft.setCursor(12, 36 + i * 20, NORM);
		tft.setTextColor(LIGHT_GREY);
		String hui = header + String("  ") + i;
		tft.print(hui);

	}
	//SetMenu(&exit);
}
void GUIClass::ListUp()
{
	if (pointer > 0) {
		pointer--;
		if (start > 0 && (((end - start) / 2)>pointer + start)) {
			start--;
			end--;
		}
	}
	else
	{
		pointer = Menu->size()-1;
		start = pointer - maxscreen;
		end = pointer;
	}
}
void GUIClass::ListDown()
{
	int size = Menu->size()-1;
	if (pointer< size) {
		pointer++;
		if (end <size&&(((end-start)/2)<pointer+start)) {
			start++;
			end++;
		}
	}
	else
	{
		pointer = 0;
		start = pointer ;
		end = maxscreen;
	}
}
void GUIClass::Click() {
	(Menu->at(pointer)->callback)(pointer);

}
void GUIClass::DrawMenu()
{
	tft.setTextScale(1);
	for (int i = start; i < end&&i<Menu->size(); i++)
	{
		if(Menu->at(i)!=NULL){
		if (pointer == i) {
			tft.drawRect(3, 19+( (i- start) * 18), 120, 16, RED);
			tft.setTextColor(BLACK, tft.htmlTo565(0x0022FF));
			tft.fillRect(4, 21 +((i - start) * 18), 118, 14, tft.htmlTo565(0x0022FF));
			tft.setCursor(6, 24 + ((i - start) * 18));
		}
		else {
			tft.drawRect(3, 20 +((i - start) * 18), 120, 16, BLUE);
			tft.setTextColor(WHITE, tft.htmlTo565(0x444444));
			tft.fillRect(4, 21 + ((i - start) * 18), 118, 14, tft.htmlTo565(0x444444));
			tft.setCursor(6, 24 + ((i - start) * 18));
		}
		tft.print(i);
		tft.print(" ");
		tft.print(Menu->at(i)->Text);
		}
	}

}
int o = 0;
void GUIClass::rm() {
	for (int i = 0; i < Menu->size(); i++)
	{
		if (Menu->at(i) != NULL) {
			delete[](Menu->at(i)->Text);
			delete[]Menu->at(i);
		}
	}
	Menu->clear();
	delete[](Menu);
	Menu= new LinkedList<menuItem*>();
	
	o++;
	Serial.println(o);
}

void GUIClass::rmrf() {
	for (int i = 0; i < Menu->size(); i++)
	{
		if (Menu->at(i) != NULL)
			delete[] (Menu->at(i)->Text);
	}
	Menu->clear();
	delete(Menu);
	delete(this);
}
void GUIClass::AddRow(char* Name, void(* callback)(int))
{
	
	Menu->push_back( new menuItem{ callback, Name });

}
void GUIClass::SetMenu(void* hui)
{
	menuItem* item = (menuItem*)hui;
	selectedMenuItem = item;
}

GUIClass GUI;

