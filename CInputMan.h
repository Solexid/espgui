// CInputMan.h

#ifndef _CInputMan_h
#define _CInputMan_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "CApp.h"

//#include "I2Cdev.h"
//#include <MPU6050.h>

#include <IRremoteESP8266.h>


class CInputMan : public CApp
{
 protected:
	// MPU6050 accelgyro;

	 //int16_t ax, ay, az;
	// int16_t gx, gy, gz;
public:
	virtual bool initApp(void* params);
	virtual void OnThink();
	virtual	void DestroyApp();
	void RenderAccel();
	void dumpCode(decode_results * results);
	void RenderFrame();
};


#endif

