// CStatusBar.h

#ifndef _CStatusBar_h
#define _CStatusBar_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "CApp.h"


class CStatusBar : public CApp
{
 protected:
	 int pheap;
	 void DrawHeap();
	 void DrawTime();

	 void DrawIcons();

 public:
	 int FullHeap;
	 bool initApp(void* params);
	 void OnThink();
};


#endif

