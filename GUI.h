// GUI.h

#ifndef _GUI_h
#define _GUI_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#include <gfxfont.h>
#include <Adafruit_GFX.h>
#include <SPI.h>
#include <TFT_ST7735.h>
extern TFT_ST7735 tft;
#include "_images/wifi2s.c"
class   menuItem {
public: void(* callback)(int i);
	 char*  (Text);
};
#include "LinkedList.h"
class GUIClass
{
 protected:

	 int maxscreen;
	 int pointer;
	 int start;
	 int end;
 public:
	LinkedList<menuItem *>  *Menu;
	char header[10]="GUI" ;
	void init(int maxitems, int height);
	virtual void MainWindow();
	void ListUp();
	void ListDown();
	void Click();
	virtual void DrawMenu();
	void rm();

	void rmrf();
	void AddRow(char* Name, void(*callback)(int));
	menuItem* selectedMenuItem; 
	virtual void SetMenu(void * hui);
};

extern GUIClass GUI;

#endif

